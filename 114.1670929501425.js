/*!
 *  build: Vue  Admin Plus
 *  copyright: vue-admin-beautiful.com
 *  time: 2022-12-13 19:05:01
 */
"use strict";(self["webpackChunkadmin_plus"]=self["webpackChunkadmin_plus"]||[]).push([[114],{60114:function(n,e,c){c.r(e),c.d(e,{default:function(){return v}});var a=c(14367);const o={class:"callback-container"};function t(n,e,c,t,s,r){return(0,a.openBlock)(),(0,a.createElementBlock)("div",o)}var s=c(54018),r=c(79204),i=c.n(r);function l(){var n=s.ZP.currentRoute.value.query;"{}"===JSON.stringify(n)&&(n=i().parse(document.location.search.slice(1))),localStorage.setItem("socialData",JSON.stringify(n))}var u=(0,a.defineComponent)({name:"Callback",setup(){const n=(0,a.inject)("$baseLoading");l(),window.open(" ","_self"),window.close(),(0,a.onUnmounted)((()=>{n.close()}))}});function f(n){n.__source="src/views/callback/index.vue"}var d=c(37068);"function"===typeof f&&f(u);const p=(0,d.Z)(u,[["render",t]]);var v=p}}]);